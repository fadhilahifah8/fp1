<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use DB;
use App\Post;

class profileController extends Controller
{
    public function index()
    {
        $postingan = Post::all();
        return view('postingan.index', compact('postingan'));
    }

    public function create()
    {
        return view('postingan.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'des' => 'required',
            'gambar'=> 'required'
        ]);

        Post::create([
            'des' => $request->des,
            'gambar' => $request->gambar
        ]);
        return redirect('/postingan');
    }

    public function show($id)
    {
        $postingan = Post::find($id);
            return view('postingan.show', compact('postingan'));
    }

    public function edit($id)
    {
        $postingan = Post::find($id);
        return view('postingan.edit', compact('postingan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'des' => 'required',
            'gambar' => 'required',
        ]);

        $postingan = Post::find($id);
        $postingan->des = $request->des;
        $postingan->gambar = $request->gambar;
        $postingan->update();

        return redirect('/postingan');
    }

    public function destroy($id)
    {
        $postingan = Post::find($id);
        $postingan->delete();
        return redirect('/postingan');
         
    }
}