<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use DB;
use App\comment;

class commentController extends Controller
{
    public function index()
    {
        $komentar = comment::all();
        return view('comment.index', compact('komentar'));
    }

    public function create()
    {
        return view('comment.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'komentar' => 'required'
            
        ]);

        comment::create([
            'komentar' => $request->komentar,

        ]);
        return redirect('/komentar');
    }

    public function show($id)
    {
        $komentar = comment::find($id);
            return view('comment.show', compact('komentar'));
    }

    public function edit($id)
    {
        $komentar= comment::find($id);
        return view('comment.edit', compact('kometar'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'komentar' => 'required',
        ]);

        $komentar = comment::find($id);
        $komentar->komentar = $request->komentar;
        $komentar->update();

        return redirect('/komentar');
    }

    public function destroy($id)
    {
        $komentar = comment::find($id);
        $komentar->delete();
        return redirect('/komentar');
    }
}