<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use DB;
use App\profile;

class profileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = profil::all();
        return view('profile.index', compact('profile'));
    }

    public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'bio'=> 'required',
            'ttl' => 'required'
        ]);

        profile::create([
            'nama' => $request->nama,
            'bio' => $request->bio,
            'ttl' => $request->ttl

        ]);
        return redirect('/profile');
    }

    public function show($id)
    {
        $profile = profile::find($id);
            return view('profile.show', compact('profile'));
    }

    public function edit($id)
    {
        $profile = profile::find($id);
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'ttl' => 'required',
        ]);

        $profile = profile::find($id);
        $profile->nama = $request->nama;
        $profile->bio = $request->bio;
        $profile->ttl = $request->ttl;
        $profile->update();
        
        return redirect('/profile');
        // $query = DB::table('profile')
        //     ->where('id', $id)
        //     ->update([
        //         'nama' => $request["nama"],
        //         'bio' => $request["bio"],
        //         'ttl' => $request["ttl"],
        //     ]);
        // return redirect('/profile');
    }

    public function destroy($id)
    {
        $profile = profile::find($id);
        $profile->delete();
        return redirect('/profile');
        // $query = DB::table('profile')->where('id', $id)->delete();
        // return redirect('/profile');
    }
}