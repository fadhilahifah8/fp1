<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/beranda', function () {
    return view('master');
});

//Route Comment
Route::get('/comment', 'commentController@index');
Route::get('/comment/create', 'commentController@create');
Route::post('/comment', 'commentController@store');
Route::get('/comment/{comment_id}', 'commentController@show');
Route::get('/comment/{comment_id}/edit', 'commentController@edit');
Route::put('/comment/{comment_id}', 'commentController@update');
Route::delete('/comment/{comment_id}', 'commentController@destroy');

//Route Profile
Route::get('/profile', 'profileController@index');
Route::get('/profile/create', 'profileController@create');
Route::post('/profile', 'profileController@store');
Route::get('/profile/{profile_id}', 'profileController@show');
Route::get('/profile/{profile_id}/edit', 'profileController@edit');
Route::put('/profile/{profile_id}', 'profileController@update');
Route::delete('/profile/{profie_id}', 'profileController@destroy');

//Route Postingan (ORM)
Route::resource('comment','commentController');
 
//Route Postingan (ORM)
Route::resource('postingan','PostController');
 
//Route Profile (ORM)
Route::resource('profile','profileController');
 


Route::get('/home', 'HomeController@index')->name('home');