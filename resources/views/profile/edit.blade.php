@extends('master')

@section('konten')
<div>
        <form action="/profile/{{$profile->id}}" method="POST">
            @method('put')
            @csrf
             <!-- nama -->
             <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <!-- bio -->
            div class="form-group">
                <label>Bio</label>
                <input type="text" class="form-control" name="bio" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <!-- ttl -->
            div class="form-group">
                <label>ttl</label>
                <input type="text" class="form-control" name="ttl" placeholder="Masukkan ttl">
                @error('ttl')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection