@extends('master')

@section('konten')
<div>
        <form action="/profile" method="POST">
            @csrf
            <!-- nama -->
            <div class="form-group ml-5 mt-3 mr-5">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <!-- bio -->
            <div class="form-group ml-5 mt-2 mr-5">
                <label>Bio</label>
                <input type="text" class="form-control" name="bio" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <!-- ttl -->
            <div class="form-group ml-5 mt-2 mr-5">
                <label>Tempat Tanggal Lahir</label>
                <input type="text" class="form-control" name="ttl" placeholder="Masukkan ttl">
                @error('ttl')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary ml-5">Tambah</button>
        </form>
</div>

@endsection