@extends('master')

<a href="/postingan/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">komentar</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($postingan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->des}}</td>
                        <td>
                            <a href="/postingan/{{$value->id}}" class="btn btn-info">Show</a>
                            <form action="/postingan/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>