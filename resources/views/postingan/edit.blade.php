@extends('master')

<div>
        <form action="/postingan/{{$postingan->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group ml-5 mt-3 mr-5">
                <label>Postingan</label>
                <input type="text" class="form-control" name="postingan" value="{{$postingan->postingan}}" placeholder="Masukkan Postingan">
                @error('postingan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
