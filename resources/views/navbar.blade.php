<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Sosial Media</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="d-flex justify-content-end"">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active btn btn-info" aria-current="page" href="/home">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active btn btn-info ml-2" aria-current="page" href="/profile">Profil</a>
              </li>
            <form class="d-flex">
              <input class="form-control ml-2 mt-1" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-info  ml-2" type="submit">Search</button>
            </form>
          </div>
      </div>
    </div>
  </nav>