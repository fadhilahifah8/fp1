@extends('master')

<div>
        <form action="/comment/{{$komentar->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Komentar</label>
                <input type="text" class="form-control" name="komentar" value="{{$komentar->komentar}}" placeholder="Masukkan Komentar">
                @error('komentar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
